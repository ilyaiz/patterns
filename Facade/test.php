<?php

/* 
 * Паттерн Фасад предоставляет унифицированный интерфейс к группе интерфейсов
 * подсистемы. Фасад определяет высокоуровневый интерфейс, упрощяющий работу с 
 * подсистемой.
 */

include __DIR__ . '/../vendor/autoload.php';

$amplifier = new \Facade\Amplifier('500 wt amplifier');

$dvdplayer = new \Facade\DVDPlayer('full hd dvd player', $amplifier);

$facade = new \Facade\HomeTheaterFacade($dvdplayer, $amplifier);

$facade->watchMovie('Die hard');

$facade->endMovie();