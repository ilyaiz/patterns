<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Facade;

/**
 * Description of HomeTheaterFacade
 *
 * @author ilya
 */
class HomeTheaterFacade {
	
	/**
	 *
	 * @var DVDPlayer
	 */
	private $dvd;
	
	/**
	 *
	 * @var Amplifier
	 */
	private $amplifier;
	
	public function __construct(DVDPlayer $dvd, Amplifier $amplifier) {
		$this->dvd = $dvd;
		$this->amplifier = $amplifier;
	}
	
	public function watchMovie(string $movie) {
		echo("Get ready to watch a movie...\n");
		$this->amplifier->on();
		$this->amplifier->setDvd($this->dvd);
		$this->amplifier->setSurroundSound();
		$this->amplifier->setVolume(5);
		$this->dvd->on();
		$this->dvd->play($movie);
	}
	
	public function endMovie() {
		echo("Shutting movie theater down...");
		$this->amplifier->off();
		$this->dvd->stop();
		$this->dvd->eject();
		$this->dvd->off();
	}
}
