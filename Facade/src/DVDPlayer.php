<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Facade;

/**
 * Description of DVDPlayer
 *
 * @author ilya
 */
class DVDPlayer {
	
	/**
	 *
	 * @var Amplifier
	 */
	private $amplifier;
	
	/**
	 *
	 * @var string
	 */
	private $description;
	
	/**
	 *
	 * @var string
	 */
	private $movie;

	public function __construct(string $description, Amplifier $amplifier) {
		$this->description = $description;
		$this->amplifier = $amplifier;
	}
	
	public function on() {
		echo($this->description . " on" . PHP_EOL);
	}
 
	public function off() {
		echo($this->description . " off" . PHP_EOL);
	}

        public function eject() {
		$this->movie = null;
                echo($this->description . " eject" . PHP_EOL);
        }
 
	public function play(string $movie) {
		$this->movie = $movie;
		$this->currentTrack = 0;
		echo($this->description . " playing \"" . $this->movie . "\"" . PHP_EOL);
	}

	public function stop() {
		$this->currentTrack = 0;
		echo($this->description . " stopped \"" . $this->movie . "\"" . PHP_EOL);
	}
 
	public function pause() {
		echo($this->description . " paused \"" . $this->movie . "\"" . PHP_EOL);
	}

	public function setTwoChannelAudio() {
		echo($this->description . " set two channel audio" . PHP_EOL);
	}
 
	public function setSurroundAudio() {
		echo($this->description . " set surround audio" . PHP_EOL);
	}
 
	public function __toString() {
		return $this->description;
	}
}
