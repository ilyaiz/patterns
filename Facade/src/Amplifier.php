<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Facade;

/**
 * Description of Amplifier
 *
 * @author ilya
 */
class Amplifier {
	
	/**
	 *
	 * @var string
	 */
	private $description;
	
	/**
	 *
	 * @var DVDPlayer 
	 */
	private $dvd;


	public function __construct(string $description) {
		$this->description = $description;
	}
	
	public function setDVD(DVDPlayer $dvd) {
		$this->dvd = $dvd;
	}
	
	public function on() {
		echo($this->description . " on" . PHP_EOL);
	}
 
	public function off() {
		echo($this->description . " off" . PHP_EOL);
	}
 
	public function setStereoSound() {
		echo($this->description . " stereo mode on" . PHP_EOL);
	}
 
	public function setSurroundSound() {
		echo($this->description . " surround sound on (5 speakers, 1 subwoofer)" . PHP_EOL);
	}
 
	public function setVolume(int $level) {
		echo($this->description . " setting volume to " . $level . PHP_EOL);
	}
	
	public function __toString() {
		return $this->description . PHP_EOL;
	}
}
