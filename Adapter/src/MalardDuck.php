<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Adapter;

/**
 * Description of MalardDuck
 *
 * @author ilya
 */
class MalardDuck implements IDuck{
	//put your code here
	public function fly() {
		echo "malard duck fly\n";
	}

	public function quack() {
		echo "quack\n";
	}

}
