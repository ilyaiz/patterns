<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Adapter;

/**
 * Description of TurkeyAdapter
 *
 * @author ilya
 */
class TurkeyAdapter implements IDuck{
	
	private $turkey;
	
	public function __construct(ITurkey $turkey) {
		$this->turkey = $turkey;
	}

	public function fly() {
		$this->turkey->fly();
	}

	public function quack() {
		$this->turkey->gobble();
	}

}
