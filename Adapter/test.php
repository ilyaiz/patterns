<?php

/* 
 * Паттерн Адаптер преобразует интерфейс класса к другому интерфейсу, на который
 * рассчитан клиент. Адаптер обеспечивает совместную работу классов, невозможную
 * в обычных условиях из-за несовместимости интерфейсов.
 */

include __DIR__ . '/../vendor/autoload.php';

$mallard_duck = new \Adapter\MalardDuck();

$wild_turkey = new Adapter\WildTurkey();

$turkey_adapter = new \Adapter\TurkeyAdapter($wild_turkey);

test_duck($mallard_duck);

test_duck($turkey_adapter);

function test_duck(\Adapter\IDuck $duck) {
	$duck->quack();
	$duck->fly();
}