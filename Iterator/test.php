<?php

/* 
 * Паттерн итератор предоставляет механизм последовательного перебора элементов
 * коллекции без раскрытия её внутреннего представления
 */

include __DIR__ . '/../vendor/autoload.php';

$waitress = new Iterator\Waitress();

$waitress->printMenu();