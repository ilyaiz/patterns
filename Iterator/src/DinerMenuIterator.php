<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of DinerMenuIterator
 *
 * @author ilya
 */
class DinerMenuIterator implements MenuIterator {
	
	const MAX_ITEMS = 5;

	/**
	 *
	 * @var array
	 */
	private $menuItems = [];
	
	private $position = 0;
	
	public function __construct(array $menuItems) {
		$this->menuItems = $menuItems;
	}
	
	/**
	 * 
	 * @return bool
	 */
	public function hasNext(): bool {
		return isset($this->menuItems[$this->position]);
	}

	
	/**
	 * 
	 * @return \Iterator\MenuItem
	 */
	public function next() : MenuItem {
		$result = NULL;

		if ($this->hasNext() && $this->position <= static::MAX_ITEMS) {

			$result = $this->menuItems[$this->position] ?? null;
			$this->position++;
		}

		return $result;
	}

}
