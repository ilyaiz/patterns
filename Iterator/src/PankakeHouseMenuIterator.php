<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of PankakeHouseMenuIterator
 *
 * @author ilya
 */
class PankakeHouseMenuIterator implements MenuIterator {
	
	/**
	 *
	 * @var ArrayList
	 */
	private $menuItems;
	
	
	private $current = 0;
	
	
	public function __construct(ArrayList $menuItems) {
		$this->menuItems = $menuItems;
	}
	
	//put your code here
	public function hasNext(): bool {
		return isset($this->menuItems[$this->current]);
	}

	public function next() : MenuItem {
		$result = NULL;

		if ($this->hasNext()) {

			$result = $this->menuItems[$this->current] ?? null;
			$this->current++;
		}

		return $result;
	}

}
