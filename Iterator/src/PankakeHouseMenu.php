<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of PankakeHouseMenu
 *
 * @author ilya
 */
class PankakeHouseMenu implements Menu {
	
	/**
	 *
	 * @var ArrayList
	 */
	private $menuItems;
	
	public function __construct() {
		$this->menuItems = new ArrayList();

		$this->addItem('Pankake', 3.00);
		$this->addItem('Waffles', 3.50);
	}
	
	/**
	 * 
	 * @return ArrayList
	 */
	public function getMenuItems() {
		return $this->menuItems;
	}

	public function getIterator(): MenuIterator {
		return new PankakeHouseMenuIterator($this->getMenuItems());
	}	

	private function addItem(string $name, float $price) {
		$this->menuItems->add(new MenuItem($name, $price));
	}


}
