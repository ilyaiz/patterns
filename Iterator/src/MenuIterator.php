<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of Iterator
 *
 * @author ilya
 */
interface MenuIterator {
	public function hasNext() : bool;
	public function next();
}
