<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of MenuItem
 *
 * @author ilya
 */
class MenuItem {
	private $name;
	private $price;
	
	public function __construct(string $name, float $price) {
		$this->name = $name;
		$this->price = $price;
	}
	
	public function __toString() {
		return "{$this->name}: \${$this->price}\n";
	}
}
