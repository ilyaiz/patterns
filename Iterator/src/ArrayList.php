<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of List
 *
 * @author ilya
 */
class ArrayList implements \ArrayAccess {
	
	private $items = [];
	
	
	public function offsetExists($offset): bool {
		return isset($this->items[$offset]);
	}

	public function offsetGet($offset) {
		return $this->items[$offset] ?? null;
	}

	public function offsetSet($offset, $value): void {
		$this->items[$offset] = $value;
	}

	public function offsetUnset($offset): void {
		unset($this->items[$offset]);
	}
	
	public function add($value) {
		$this->items[] = $value;
	}

}
