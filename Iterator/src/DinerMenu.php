<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of DinerMenu
 *
 * @author ilya
 */
class DinerMenu implements Menu {
	const MAX_ITEMS = 5;
	
	/**
	 *
	 * @var array
	 */
	private $menuItems = [];
	
	public function __construct() {
		
		$this->addItem('Soup', 2.00);
		$this->addItem('Hot-Dog', 1.00);
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getMenuItems() {
		return $this->menuItems;
	}
	
	/**
	 * 
	 * @return \Iterator\MenuIterator
	 */
	public function getIterator(): MenuIterator {
		return new DinerMenuIterator($this->getMenuItems());
	}

	private function addItem(string $name, float $price) {
		if (count($this->menuItems) < static::MAX_ITEMS) {
			$this->menuItems[] = new MenuItem($name, $price);
		}
	}

	

}
