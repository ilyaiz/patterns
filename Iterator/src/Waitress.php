<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iterator;

/**
 * Description of Waitress
 *
 * @author ilya
 */
class Waitress {
	
	private $pankakeMenu;
	private $dinerMenu;
	
	public function __construct() {
		$this->pankakeMenu = new PankakeHouseMenu();
		$this->dinerMenu = new DinerMenu();
	}
	
	public function printMenu() {
		$pankakeIterator = $this->pankakeMenu->getIterator();
		$dinerIterator = $this->dinerMenu->getIterator();

		$this->printMenuItems($pankakeIterator);
		$this->printMenuItems($dinerIterator);
	}
	
	private function printMenuItems(MenuIterator $iterator) {
		while($iterator->hasNext()) {
			echo (string)$iterator->next();
		}
	}
}
