<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Decorator;

/**
 * Description of Espresso
 *
 * @author ilya
 */
class HouseBlend extends Beverage {
	
	public function __construct() {
		$this->description  = "House Blend";
	}
	
	public function cost(): float {
		return 0.89;
	}

	public function getDescription(): string {
		return $this->description;
	}

}
