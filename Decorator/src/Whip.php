<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Decorator;

/**
 * Description of Mocha
 *
 * @author ilya
 */
class Whip extends CondimentDecorator{
	
	/**
	 *
	 * @var Beverage 
	 */
	private $beverage;

	public function __construct(Beverage $beverage) {
		$this->beverage = $beverage;
	}
	
	public function getDescription() : string {
		return $this->beverage->getDescription() . ", Whip";
	}

	public function cost(): float {
		return 0.9 + $this->beverage->cost();
	}

}
