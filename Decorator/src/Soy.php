<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Decorator;

/**
 * Description of Mocha
 *
 * @author ilya
 */
class Soy extends CondimentDecorator{
	
	/**
	 *
	 * @var Beverage 
	 */
	private $beverage;

	public function __construct(Beverage $beverage) {
		$this->beverage = $beverage;
	}
	
	public function getDescription() : string {
		return $this->beverage->getDescription() . ", Soy";
	}

	public function cost(): float {
		return 0.10 + $this->beverage->cost();
	}

}
