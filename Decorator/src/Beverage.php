<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Decorator;

/**
 * Description of Beverage
 *
 * @author ilya
 */
abstract class Beverage {
	protected $description = "Unknown description";
	
	public abstract function getDescription() : string;

	public abstract function cost() : float;
	
	public function getDescriptionWithCost() {
		return $this->getDescription() . " \$" . $this->cost() . PHP_EOL;
	}
}
