<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Decorator;

/**
 * Description of Espresso
 *
 * @author ilya
 */
class Espresso extends Beverage {
	
	public function __construct() {
		$this->description  = "Espresso";
	}
	
	public function cost(): float {
		return 1.99;
	}

	public function getDescription(): string {
		return $this->description;
	}

}
