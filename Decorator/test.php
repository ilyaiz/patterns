<?php

/**
 * Паттерн Декоратор динамически наделяет обьект новыми возможностями,
 * и является гибкой альтернативой субклассированию в области расширения 
 * функциональности
 */


include __DIR__ . '/../vendor/autoload.php';

$beverage = new Decorator\Espresso();

echo $beverage->getDescriptionWithCost();

$beverage2 = new \Decorator\Soy(
	new Decorator\Mocha(
		$beverage
	)
);

echo $beverage2->getDescriptionWithCost();