<?php

/**
 * Паттерн Наблюдатель определяет отношение "один ко многим" между обьектами
 * таким образом, что при изменении состояния одного обьекта происходит автоматическое
 * оповещение и обновление всех зависимых обьектов
 */


include __DIR__ . '/../vendor/autoload.php';

$weatherData = new \Observers\WeatherData();

$currentConditionsDisplay = new \Observers\CurrentConditionsDisplay($weatherData);

$headIndexDisplay = new \Observers\HeatIndexDisplay($weatherData);

$weatherData->setMeasurements(10, 50, 200);

$weatherData->removeObserver($headIndexDisplay);

$weatherData->setMeasurements(12, 50, 200);