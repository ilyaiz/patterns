<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Observers;
/**
 * Description of IObserver
 *
 * @author ilya
 */
interface IObserver {
	public function update(int $temperature, float $humidity, float $pressure);
}
