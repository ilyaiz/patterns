<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Observers;
/**
 *
 * @author ilya
 */
interface IObservable {
	public function registerObserver(IObserver $observer);
	public function removeObserver(IObserver $observer);
	public function notifyObservers();
}
