<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Observers;

/**
 * Description of WeatherData
 *
 * @author ilya
 */
class WeatherData implements IObservable {
	
	/**
	 *
	 * @var array 
	 */
	private $observers = [];
	
	private $temperature;
	
	private $humidity;
	
	private $pressure;
	
	public function notifyObservers() {
		foreach ($this->observers as &$observer) {
			$observer->update($this->temperature, $this->humidity, $this->pressure);
		}
	}

	public function registerObserver(IObserver $observer) {
		$this->observers[] = $observer;
	}

	public function removeObserver(IObserver $observer) {
		
		$remove_key = $this->_findObserverKey($observer);
		
		if ($remove_key != null && array_key_exists($remove_key, $this->observers)) {
			unset($this->observers[$remove_key]);
		}
	}
	
	public function measurementsChanged() {
		$this->notifyObservers();
	}
	
	public function setMeasurements($temperature, $humidity, $pressure) {
		$this->temperature = $temperature;
		$this->humidity = $humidity;
		$this->pressure = $pressure;
		
		$this->measurementsChanged();
	}
	
	
	private function _findObserverKey(IObserver $observer) {

		$remove_key = null;
		//TODO: продебажить
		foreach ($this->observers as $key => &$stored_observer) {
			if ($stored_observer === $observer) {
				$remove_key = $key;
				break;
			}
		}
		
		return $remove_key;
	}

}
