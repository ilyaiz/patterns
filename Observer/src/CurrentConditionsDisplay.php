<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Observers;

/**
 * Description of CurrentConditionsDisplay
 *
 * @author ilya
 */
class CurrentConditionsDisplay implements IObserver, IDisplayElement {
	
	private $temperature;
	
	private $humidity;
	
	private $pressure;
	
	/**
	 *
	 * @var ISubject
	 */
	private $weatherData;
	
	public function __construct(IObservable $weatherData) {
		$this->weatherData = $weatherData;
		$this->weatherData->registerObserver($this);
	}
	
	
	public function display() {
		echo "Текущая погода: {$this->temperature} градусов по цельсию, ",
			"влажность {$this->humidity}, ",
			"давление {$this->pressure}.\n";
	}

	public function update(int $temperature, float $humidity, float $pressure) {
		$this->temperature = $temperature;
		$this->humidity = $humidity;
		$this->pressure = $pressure;
		$this->display();
	}

}
