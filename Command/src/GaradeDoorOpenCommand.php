<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Command;

/**
 * Description of GaradeDoorOpenCommand
 *
 * @author ilya
 */
class GaradeDoorOpenCommand implements \Command\Command{
	
	/**
	 *
	 * @var GarageDoor
	 */
	private $garageDoor;


	public function __construct(GarageDoor $door) {
		$this->garageDoor = $door;
	}


	//put your code here
	public function execute() {
		$this->garageDoor->up();
		$this->garageDoor->lightOn();
	}

	public function undo() {
		$this->garageDoor->lightOff();
		$this->garageDoor->down();
	}

}
