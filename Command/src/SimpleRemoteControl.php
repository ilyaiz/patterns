<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Command;

/**
 * Description of SimpleRemoteControl
 *
 * @author ilya
 */
class SimpleRemoteControl {
	
	/**
	 *
	 * @var Command
	 */
	private $slot;
	
	public function setCommand(\Command\Command $command) {
		$this->slot = $command;
	}
	
	public function buttonOnPressed() {
		$this->slot->execute();
	}
	
	public function buttonOffPressed() {
		$this->slot->undo();
	}
}
