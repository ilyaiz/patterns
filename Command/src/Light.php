<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Command;

/**
 * Description of Light
 *
 * @author ilya
 */
class Light {
	public function on() {
		echo "Light on\n";
	}
	
	public function off() {
		echo "Light off\n";
	}
}
