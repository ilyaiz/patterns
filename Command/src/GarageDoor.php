<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Command;

/**
 * Description of GarageDoor
 *
 * @author ilya
 */
class GarageDoor {
	public function up() {echo "Garage door up" . PHP_EOL;}
	public function down() {echo "Garage door down" . PHP_EOL;}
	public function stop() {echo "Garage door stop" . PHP_EOL;}
	public function lightOn() {echo "Garage door lightOn" . PHP_EOL;}
	public function lightOff() {echo "Garage door lightOff" . PHP_EOL;}
}
