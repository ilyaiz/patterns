<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Command;

/**
 * Description of LightOnCommand
 *
 * @author ilya
 */
class LightOnCommand implements \Command\Command{
	
	private $light;

	public function __construct(\Command\Light $light) {
		$this->light = $light;
	}

	public function execute() {
		$this->light->on();
	}

	public function undo() {
		$this->light->off();
	}

}
