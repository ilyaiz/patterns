<?php

/* 
 * Паттерн Комманда инкапсулирует запрос в виде обьекта, делая возможной
 * параметризацию клиентских обьектов с другими запросами, организацию очереди
 * или регистрацию запросов, а также поддержку отмены операций.
 */

include __DIR__ . '/../vendor/autoload.php';

$simpleRemoteControl = new Command\SimpleRemoteControl();

$light = new Command\Light();

$lightOnCommand = new Command\LightOnCommand($light);

$simpleRemoteControl->setCommand($lightOnCommand);

$simpleRemoteControl->buttonOnPressed();

$simpleRemoteControl->buttonOffPressed();

$garageDoor = new Command\GarageDoor();

$garageOpenCommand = new Command\GaradeDoorOpenCommand($garageDoor);

$simpleRemoteControl->setCommand($garageOpenCommand);

$simpleRemoteControl->buttonOnPressed();

$simpleRemoteControl->buttonOffPressed();