<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory;

/**
 * Description of CheesePizza
 *
 * @author ilya
 */
class CheesePizza extends Pizza{
	
	/**
	 *
	 * @var PizzaIngridientFactory\PizzaIngridientFactory
	 */
	protected $pizzaIngridientFactory;
	
	public function __construct(PizzaIngridientFactory\PizzaIngridientFactory $pizzaIngridientFactory) {
		$this->pizzaIngridientFactory = $pizzaIngridientFactory;
	}
	
	public function prepare() {
		$this->dough = $this->pizzaIngridientFactory->createDough();
		$this->sauce = $this->pizzaIngridientFactory->createSauce();
		$this->cheese = $this->pizzaIngridientFactory->createCheese();
		$this->veggies = $this->pizzaIngridientFactory->createVeggies();
	}

}
