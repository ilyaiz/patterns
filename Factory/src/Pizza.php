<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Factory\PizzaIngridientFactory;

namespace Factory;

/**
 * Description of Pizza
 *
 * @author ilya
 */
abstract class Pizza {
	
	/**
	 *
	 * @var string
	 */
	protected $name = "abstract pizza";
	
	/**
	 *
	 * @var PizzaIngridientFactory\Dough\Dough
	 */
	protected $dough;
	
	/**
	 *
	 * @var PizzaIngridientFactory\Sauce\Sauce
	 */
	protected $sauce;

	/**
	 *
	 * @var [PizzaIngridientFactory\Veggies\Veggies]
	 */
	protected $veggies = [];
	
	/**
	 *
	 * @var PizzaIngridientFactory\Cheese\Cheese
	 */
	protected $cheese;
	
	/**
	 *
	 * @var PizzaIngridientFactory\Pepperoni\Pepperoni
	 */
	protected $pepperoni;
	
	
	/**
	 *
	 * @var PizzaIngridientFactory\Clams\Clams
	 */
	protected $clam;


	public abstract function prepare();

	public function bake() {
		echo "Bake for 25 minutes at 350\n";
	}

	public function cut() {
		echo "Cutting the pizza into diagonal slices\n";
	}

	public function box() {
		echo "Place pizza in official PizzaStore box\n";
	}
	
	public function setName(string $name) {
		$this->name = $name;
	}
	
	public function getName() : string {
		return $this->name;
	}
	
	public function __toString() {
		$result = "";
		$result .= "---- {$this->name} ----\n";
		if ($this->dough) {
			$result .= $this->dough . PHP_EOL;
		}
		if ($this->sauce) {
			$result .= $this->sauce . PHP_EOL;
		}
		if ($this->cheese) {
			$result .= $this->cheese . PHP_EOL;
		}
		if (!empty($this->veggies)) {
			$result .= "Veggies: ";
			$result .= implode(', ', $this->veggies);
			$result .= PHP_EOL;
		}
		if ($this->clam) {
			$result .= $this->clam . PHP_EOL;
		}
		if ($this->pepperoni) {
			$result .= $this->pepperoni . PHP_EOL;
		}
		return $result;
	}
}
