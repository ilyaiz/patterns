<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Clams;

/**
 * Description of FrozenClams
 *
 * @author ilya
 */
class FrozenClams implements Clams{
	//put your code here
	public function __toString(): string {
		return "Frozen clams";
	}

}
