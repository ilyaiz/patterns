<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Dough;

/**
 * Description of ThickCrustDough
 *
 * @author ilya
 */
class ThickCrustDough implements Dough{
	//put your code here
	public function __toString(): string {
		return "Thick crust dough";
	}

}
