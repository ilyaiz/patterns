<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Dough;

/**
 * Description of ThinCrustDough
 *
 * @author ilya
 */
class ThinCrustDough implements Dough {
	public function __toString() : string {
		return "Thin Crust Dough";
	}
}
