<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Veggies;

/**
 *
 * @author ilya
 */
interface Veggies {
	public function __toString() : string;
}
