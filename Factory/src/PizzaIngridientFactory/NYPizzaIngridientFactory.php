<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory;

/**
 * Description of NYPizzaIngridientFactory
 *
 * @author ilya
 */
class NYPizzaIngridientFactory implements PizzaIngridientFactory {
	
	
	public function createCheese(): Cheese\Cheese {
		return new Cheese\ReggianoCheese();
	}

	public function createClam(): Clams\Clams {
		return new Clams\FreshClams();
	}

	public function createDough(): Dough\Dough {
		return new Dough\ThinCrustDough();
	}

	public function createPepperoni(): Pepperoni\Pepperoni {
		return new Pepperoni\SlicedPepperoni();
	}

	public function createSauce(): Sauce\Sauce {
		return new Sauce\MarinaraSauce();
	}

	public function createVeggies(): array {
		return [new Veggies\Garlic()];
	}

}
