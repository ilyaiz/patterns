<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Cheese;

/**
 * Description of Mozzarella
 *
 * @author ilya
 */
class Mozzarella implements Cheese{
	//put your code here
	public function __toString(): string {
		return "Mozarella";
	}

}
