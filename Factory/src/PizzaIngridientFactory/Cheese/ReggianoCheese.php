<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Cheese;

/**
 * Description of ReggianoCheese
 *
 * @author ilya
 */
class ReggianoCheese implements Cheese {

	public function __toString() : string {
		return "Reggiano Cheese";
	}
}
