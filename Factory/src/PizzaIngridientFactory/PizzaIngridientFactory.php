<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory;

/**
 *
 * @author ilya
 */
interface PizzaIngridientFactory {
	
	public function createDough() : Dough\Dough;
	
	public function createSauce() : Sauce\Sauce;
	
	public function createCheese() : Cheese\Cheese;
	
	public function createVeggies() : array;
	
	public function createPepperoni() : Pepperoni\Pepperoni;
	
	public function createClam() : Clams\Clams;
}
