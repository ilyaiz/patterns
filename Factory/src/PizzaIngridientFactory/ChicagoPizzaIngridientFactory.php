<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory;

/**
 * Description of ChicagoPizzaIngridientFactory
 *
 * @author ilya
 */
class ChicagoPizzaIngridientFactory implements PizzaIngridientFactory{
	
	public function createCheese(): Cheese\Cheese {
		return new Cheese\Mozzarella();
	}

	public function createClam(): Clams\Clams {
		return new Clams\FrozenClams();
	}

	public function createDough(): Dough\Dough {
		return new Dough\ThickCrustDough();
	}

	public function createPepperoni(): Pepperoni\Pepperoni {
		return new Pepperoni\SlicedPepperoni();
	}

	public function createSauce(): Sauce\Sauce {
		return new Sauce\PlumTomatoSauce();
	}

	public function createVeggies(): array {
		return [new Veggies\BlackOlives(), new Veggies\Spinach(), new Veggies\Eggplant()];
	}

}
