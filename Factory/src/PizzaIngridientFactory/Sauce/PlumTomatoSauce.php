<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory\PizzaIngridientFactory\Sauce;

/**
 * Description of PlumTomatoSauce
 *
 * @author ilya
 */
class PlumTomatoSauce implements Sauce{
	//put your code here
	public function __toString(): string {
		return "Plum tomato sauce";
	}

}
