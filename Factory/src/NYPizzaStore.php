<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Factory;

/**
 * Description of NYPizzaStore
 *
 * @author ilya
 */
class NYPizzaStore extends PizzaStore{
	
	protected function createPizza(string $type): Pizza {

		$pizza = null;
		
		$pizzaIngridientFactory = new PizzaIngridientFactory\NYPizzaIngridientFactory();
		
		$name = "NY style $type pizza";
		
		if ($type == "cheese") {
			$pizza = new CheesePizza($pizzaIngridientFactory);
		} else if ($type == "clam") {
			$pizza = new ClamPizza($pizzaIngridientFactory);
		} else {
			throw new Exception("Unknown pizza type '$type'");
		}
		
		$pizza->setName($name);
		
		return $pizza;
	}

}
