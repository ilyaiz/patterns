<?php

/* 
 * Паттерн Абстрактнаяя Фабрика предоставляет интерфейс создания семейств
 * взаимосвязанных или взаимозависимых обьектов без указания их конкретных классов
 */

include __DIR__ . '/../vendor/autoload.php';

$nyPizzaStore = new Factory\NYPizzaStore();

$chicagoPizzaStore = new Factory\ChicagoPizzaStore();

$ny_cheese_pizza = $nyPizzaStore->orderPizza('cheese');

echo $ny_cheese_pizza;

$chicago_cheese_pizza = $chicagoPizzaStore->orderPizza('cheese');

echo $chicago_cheese_pizza;

$ny_clam_pizza = $nyPizzaStore->orderPizza('clam');

echo $ny_clam_pizza;

$chicago_clam_pizza = $chicagoPizzaStore->orderPizza('clam');

echo $chicago_clam_pizza;