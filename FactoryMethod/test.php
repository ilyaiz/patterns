<?php

/* 
 * Паттерн Фабричный метод определяет интерфейс создания обьекта, но позволяет
 * субклассам выбирать класс создаваемого экземпляра. Таким образом, Фабричный метод
 * делегирует операцию создания экземпляра субклассам.
 */

include __DIR__ . '/../vendor/autoload.php';

$nyPizzaStore = new FactoryMethod\NYPizzaStore();

$chicagoPizzaStore = new FactoryMethod\ChicagoPizzaStore();


$pizza1 = $nyPizzaStore->orderPizza('cheese');

$pizza2 = $chicagoPizzaStore->orderPizza('cheese');