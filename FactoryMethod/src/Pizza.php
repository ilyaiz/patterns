<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FactoryMethod;

/**
 * Description of Pizza
 *
 * @author ilya
 */
abstract class Pizza {
	
	protected $name = "abstract pizza";
	protected $dough = "abstract dough";
	protected $sauce = "abstract sauce";
	protected $toppings = [];
	
	public function prepare() {
		echo "preparing {$this->name}\n";
	}

	public function bake() {
		echo "Bake for 25 minutes at 350\n";
	}

	public function cut() {
		echo "Cutting the pizza into diagonal slices\n";
	}

	public function box() {
		echo "Place pizza in official PizzaStore box\n";
	}
}
