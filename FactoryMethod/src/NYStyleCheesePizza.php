<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FactoryMethod;

/**
 * Description of NYStyleCheesePizza
 *
 * @author ilya
 */
class NYStyleCheesePizza extends Pizza {
	public function __construct() {
		$this->name = "NY Style Sauce and Cheese Pizza";
		$this->dough = "Thin crust dough";
		$this->sauce = "Marinara";
		$this->toppings[] = "Grated reggiano cheese";
	}
}
