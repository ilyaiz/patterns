<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FactoryMethod;

/**
 * Description of NYPizzaStore
 *
 * @author ilya
 */
class NYPizzaStore extends PizzaStore{
	
	protected function createPizza(string $type): Pizza {
		$pizza = null;
		
		if ($type == "cheese") {
			$pizza = new NYStyleCheesePizza();
		} else {
			throw new Exception("Unknown pizza type '$type'");
		}
		
		return $pizza;
	}

}
