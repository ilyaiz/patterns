<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FactoryMethod;

/**
 * Description of PizzaStore
 *
 * @author ilya
 */
abstract class PizzaStore {
	
	public function orderPizza(string $type) : Pizza {
		$pizza = $this->createPizza($type);
		
		$pizza->prepare();
		$pizza->bake();
		$pizza->cut();
		$pizza->box();
		
		return $pizza;
	}
	
	protected abstract function createPizza(string $type) : Pizza;
	
}
