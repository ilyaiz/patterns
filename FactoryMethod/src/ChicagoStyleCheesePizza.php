<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FactoryMethod;

/**
 * Description of NYStyleCheesePizza
 *
 * @author ilya
 */
class ChicagoStyleCheesePizza extends Pizza {
	public function __construct() {
		$this->name = "Chicago Style Deep Dish Cheese Pizza";
		$this->dough = "Extra tick crust dough";
		$this->sauce = "Plum tomato sauce";
		$this->toppings[] = "Shredded mozzarella cheese";
	}
	
	public function cut() {
		echo "Cutting the pizza into square slices\n";
	}
}
