<?php

/*
 * Composite
 */

namespace AllInOne;

/**
 * Description of Flock
 *
 * @author ilya
 */
class Flock extends Quackable{
	
	private $ducks = [];
	
	public function add(Quackable $duck) {
		$this->ducks[] = $duck;
	}
	//put your code here
	public function quack() {
		foreach ($this->ducks as $duck) {
			$duck->quack();
		}
	}

	public function registerObserver(Observer $observer) {
		foreach ($this->ducks as $duck) {
			$duck->registerObserver($observer);
		}
	}

	public function __toString() {
		return "Flock";
	}

}
