<?php

/*
 * Abstract Factory
 */

namespace AllInOne;

/**
 * Description of DuckFactory
 *
 * @author ilya
 */
class CountingDuckFactory extends AbstractDuckFactory{
	//put your code here
	public function createDuckCall(): Quackable {
		return new QuackCounter(new DuckCall());
	}

	public function createMallardDuck(): Quackable {
		return new QuackCounter(new MallardDuck());
	}

	public function createRedheadDuck(): Quackable {
		return new QuackCounter(new RedheadDuck());
	}

	public function createRubberDuck(): Quackable {
		return new QuackCounter(new RubberDuck());
	}
	
	public function createGooseAdapter(): Quackable {
		return new QuackCounter(new GooseAdapter(new Goose()));
	}

}
