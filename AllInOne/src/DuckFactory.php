<?php

/*
 * Abstract Factory
 */

namespace AllInOne;

/**
 * Description of DuckFactory
 *
 * @author ilya
 */
class DuckFactory extends AbstractDuckFactory{
	//put your code here
	public function createDuckCall(): Quackable {
		return new DuckCall();
	}

	public function createMallardDuck(): Quackable {
		return new MallardDuck();
	}

	public function createRedheadDuck(): Quackable {
		return new RedheadDuck();
	}

	public function createRubberDuck(): Quackable {
		return new RubberDuck();
	}

	public function createGooseAdapter(): Quackable {
		return new GooseAdapter(new Goose());
	}

}
