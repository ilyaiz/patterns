<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AllInOne;

/**
 * Description of Observable
 *
 * @author ilya
 */
class Observable implements QuackObservable{
	
	/**
	 *
	 * @var array
	 */
	private $observers = [];
	
	/**
	 *
	 * @var QuackObservable 
	 */
	private $duck;
	
	public function __construct(QuackObservable $duck) {
		$this->duck = $duck;
	}

	public function notifyObservers() {

		foreach ($this->observers as $observer) {
			$observer->update($this->duck);
		}
	}

	public function registerObserver(Observer $observer) {
		$this->observers[] = $observer;
	}

}
