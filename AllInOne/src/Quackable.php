<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AllInOne;
/**
 *
 * @author ilya
 */
abstract class Quackable implements QuackObservable{

	public abstract function quack();
	
	/**
	 *
	 * @var Observable	
	 */
	private $observable;
	
	
	public function __construct() {
		$this->observable = new Observable($this);
	}
	
	public function notifyObservers() {
		$this->observable->notifyObservers();
	}

	public function registerObserver(Observer $observer) {
		$this->observable->registerObserver($observer);
	}
	
	public abstract function __toString();
}
