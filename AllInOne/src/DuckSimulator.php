<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AllInOne;

/**
 * Description of DuckSimulator
 *
 * @author ilya
 */
class DuckSimulator {
	public function simulate(AbstractDuckFactory $duckFactory) {
		
		$redheadDuck = $duckFactory->createRedheadDuck();
		$duckCall = $duckFactory->createDuckCall();
		$rubberDuck = $duckFactory->createRubberDuck();
		$gooseDuck = $duckFactory->createGooseAdapter();
		
		echo "Duck simulator\n";
		
		
		
		$flockOfDucks = new Flock();
		$flockOfDucks->add($redheadDuck);
		$flockOfDucks->add($duckCall);
		$flockOfDucks->add($rubberDuck);
		$flockOfDucks->add($gooseDuck);

		

		$flockOfMallards = new Flock();
		
		$mallardDuckOne = $duckFactory->createMallardDuck();
		$mallardDuckTwo = $duckFactory->createMallardDuck();
		$mallardDuckThree = $duckFactory->createMallardDuck();
		$mallardDuckFour = $duckFactory->createMallardDuck();
		
		$flockOfMallards->add($mallardDuckOne);
		$flockOfMallards->add($mallardDuckTwo);
		$flockOfMallards->add($mallardDuckThree);
		$flockOfMallards->add($mallardDuckFour);
		
		$flockOfDucks->add($flockOfMallards);
		
		$quackologist = new Quackologist();
		$flockOfDucks->registerObserver($quackologist);
		
		echo "Flock of mallards:\n";
		$this->_simulate($flockOfMallards);
		
		echo "Flock of all: \n";
		$this->_simulate($flockOfDucks);
		
		echo "Quack count: " . QuackCounter::getQuacksCount() . PHP_EOL;
	}
	
	private function _simulate(Quackable $duck) {
		$duck->quack();
	}
}
