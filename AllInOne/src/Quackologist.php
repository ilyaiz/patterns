<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AllInOne;

/**
 * Description of Quackologist
 *
 * @author ilya
 */
class Quackologist implements Observer{
	//put your code here
	public function update(QuackObservable $duck) {
		echo "Quackologist: duck " . $duck . " just quacked\n";
	}

}
