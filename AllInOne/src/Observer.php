<?php

/*
 * Observer
 */

namespace AllInOne;

/**
 *
 * @author ilya
 */
interface Observer {
	public function update(QuackObservable $duck);
}
