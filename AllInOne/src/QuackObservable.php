<?php

/*
 * Observer
 */

namespace AllInOne;

/**
 *
 * @author ilya
 */
interface QuackObservable {
	public function registerObserver(Observer $observer);
	public function notifyObservers();
	
}
