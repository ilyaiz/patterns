<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AllInOne;

/**
 * Description of RubberDuck
 *
 * @author ilya
 */
class RubberDuck extends Quackable{
	//put your code here
	
	public function quack() {
		echo "Squeak\n";
		$this->notifyObservers();
	}

	public function __toString() {
		return "Rubber duck";
	}

}
