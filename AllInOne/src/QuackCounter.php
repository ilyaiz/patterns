<?php

/*
 * Decorator
 */

namespace AllInOne;

/**
 * Description of QuackCounter
 *
 * @author ilya
 */
class QuackCounter extends Quackable{
	
	private $duck;
	private static $quacksCount = 0;
	
	public function __construct(Quackable $duck) {
		$this->duck = $duck;
		parent::__construct();
	}
	//put your code here
	public function quack() {
		static::$quacksCount++;
		$this->duck->quack();
		$this->notifyObservers();
	}
	
	public static function getQuacksCount() {
		return static::$quacksCount;
	}

	public function __toString() {
		return (string)$this->duck;
	}

}
