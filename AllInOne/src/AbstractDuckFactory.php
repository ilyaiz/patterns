<?php

/*
 * Abstract Factory
 */

namespace AllInOne;

/**
 * Description of AbstractDuckFactory
 *
 * @author ilya
 */
abstract class AbstractDuckFactory {
	public abstract function createMallardDuck() : Quackable;
	public abstract function createRedheadDuck() : Quackable;
	public abstract function createDuckCall() : Quackable;
	public abstract function createRubberDuck() : Quackable;
	public abstract function createGooseAdapter(): Quackable;
}
