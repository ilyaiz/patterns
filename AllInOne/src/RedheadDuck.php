<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AllInOne;

/**
 * Description of RedheadDuck
 *
 * @author ilya
 */
class RedheadDuck extends Quackable{
	//put your code here
	public function quack() {
		echo "Quack\n";
		$this->notifyObservers();
	}

	public function __toString() {
		return "Redhead duck";
	}

}
