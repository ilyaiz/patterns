<?php

/*
 * Adapter
 */

namespace AllInOne;

/**
 * Description of GooseAdapter
 *
 * @author ilya
 */
class GooseAdapter extends Quackable{
	
	/**
	 *
	 * @var Goose
	 */
	private $_goose;
	
	public function __construct(Goose $goose) {
		parent::__construct();
		$this->_goose = $goose;
	}
	//put your code here
	public function quack() {
		$this->_goose->honk();
		$this->notifyObservers();
	}

	public function __toString() {
		return "Goose";
	}

}
