<?php

/* 
 * Все паттерны собранные в кучу
 */

include __DIR__ . '/../vendor/autoload.php';

$duckFactory = new AllInOne\CountingDuckFactory();

$duckSimulator = new AllInOne\DuckSimulator();

$duckSimulator->simulate($duckFactory);

