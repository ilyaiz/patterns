<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Ducks;
/**
 * Description of MalardDuck
 *
 * @author ilya
 */
class MalardDuck extends Duck{
	
	public function __construct() {
		$this->fly_behavior = new FlyWithWings();
		
		$this->quack_behavior = new Quack();
	}

}
