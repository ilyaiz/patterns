<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Ducks;
/**
 * Description of Duck
 *
 * @author ilya
 */
abstract class Duck {
	
	/**
	 *
	 * @var FlyBehavior 
	 */
	protected $fly_behavior;
	
	/**
	 *
	 * @var QuackBehavior
	 */
	protected $quack_behavior;
	
	public function display() {
		echo __CLASS__ . PHP_EOL;
	}
	
	public function performFly() {
		$this->fly_behavior->fly();
	}
	
	public function performQuack() {
		$this->quack_behavior->quack();
	}
	
	public function swim() {
		echo "All ducks are swim\n";
	}
	
	public function setFlyBehavior(FlyBehavior $behavior) {
		$this->fly_behavior = $behavior;
	}
	
	public function setQuackBehavior(QuackBehavior $behavior) {
		$this->quack_behavior = $behavior;
	}
}
