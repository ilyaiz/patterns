<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Ducks;
/**
 * Description of Squeak
 *
 * @author ilya
 */
class Squeak implements QuackBehavior{
	
	public function quack() {
		echo "squeak!\n";
	}

}
