<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Ducks;
/**
 * Description of ModelDuck
 *
 * @author ilya
 */
class ModelDuck extends Duck{
	
	public function __construct() {
		$this->setFlyBehavior(new NoFly());
		$this->setQuackBehavior(new MuteQuack());
	}

}
